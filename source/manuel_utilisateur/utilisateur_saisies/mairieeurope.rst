#######################
Mention "mairie europe"
#######################

Depuis la mise en place du Répertoire Électoral Unique au 1er janvier 2019, cette mention n'est plus saisie dans openElec. Une mention reste aposée sur la liste d'émargement mais c'est le Répertoire Électoral Unique qui en est à l'origine pas la commune.

