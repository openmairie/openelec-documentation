#####################################
Mention "centre de vote à l'étranger"
#####################################

Depuis la mise en place du Répertoire Électoral Unique au 1er janvier 2019, cette mention n'existe plus dans openElec.

"A compter de 2019, les Français résidant à l’étranger ne pourront plus être inscrits à la fois sur une liste électorale consulaire pour les scrutins nationaux, et sur une liste électorale municipale pour les scrutins locaux. Ceux qui sont actuellement inscrits (2018) sur deux listes devront donc choisir sur laquelle ils se maintiennent en 2019. Ils ne pourront plus voter qu’à un seul endroit pour tous les scrutins qu’ils soient nationaux ou locaux.

S’ils souhaitent voter en France, ils devront obligatoirement demander leur radiation de la liste électorale consulaire sur service-public.fr : inscription consulaire/actualiser son dossier en cours de séjour.

Par défaut, les Français résidant à l’étranger inscrits à la fois sur une liste en France et sur une liste électorale consulaire seront maintenus sur la liste électorale consulaire et radiés de la liste de leur commune en France. Ils voteront donc à l’étranger pour tous les scrutins."

