.. _module_arrets_listes:

##########################
Module "Arrêts des listes"
##########################

Le module "Arrêts des listes" est accessible via le menu
(:menuselection:`Traitement --> Arrêts des listes`).

.. image:: a_module_arrets_liste_menu.png

.. contents::


Préambule
=========

Les fonctionnalités du module "Arrêts des listes" :

 - Demande d'arrêt des listes pour une année sans scrutin
 - Demande d'arrêt des listes pour un scrutin
 - Consultation du statut des arrêts des listes
 - Téléchargement du livrable d'arrêt des listes
 - Génération et consultation des listes électorales


Fonctionnement
==============

La demande d':ref:`arrêt des listes pour une élection<module_election_arrets_listes>` ainsi que :ref:`les éditions<edition_livrable_j20_module_election>` ont le même fonctionnement dans ce module que dans le module scrutin.

En revanche, il est possible (uniquement depuis ce module) de faire une demande d'arrêt des listes pour une année sans scrutin et de consulter le statut des arrêts des listes.


Statut des arrêts des listes
----------------------------

Depuis le listing de ce module, la colonne **statut** permet de connaître l'état de la demande d'arrêt des listes. Les statuts possibles sont les suivants :

    - |demande_non_disponible| : Trop tôt pour faire la demande de livrable.
    - |demande_possible| : Demande possible.
    - |demande_retard| : Trop tard pour faire la demande.
    - |attente_livrable| : Livrable demandé et en attente de réception.
    - |livrable_recu| : Livrable reçu.


Demande d'arrêt des listes pour une année sans élection
-------------------------------------------------------

Dans le cadre d'une année sans scrutin, l'arrêt des listes doit être effectué en fin d'année.
Pour en faire la demande, il faut simplement cliquer sur la croix verte puis cliquer sur ajouter.

Depuis le listing, une nouvelle ligne "Arrêt des listes hors élections" sera visible. De la même manière que pour un arrêt des listes "classique", le téléchargement du livrable et les éditions seront accessibles depuis la page de consultation de l'arrêt.

.. image:: a_module_arrets_liste_listing.png

.. Raccourcis pour avoir des images affichées en ligne

.. |demande_non_disponible| image:: i_listeemargement-grey.png
.. |demande_possible| image:: i_listeemargement-25x25.png
.. |demande_retard| image:: i_erreur.png 
.. |attente_livrable| image:: i_nonarrive.png
.. |livrable_recu| image:: i_arrive.png