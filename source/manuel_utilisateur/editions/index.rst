.. _editions:

########
Editions
########


.. figure:: a_menu-rubrik-edition.png

   Menu - Rubrique 'Édition'

Editions par bureau
-------------------

Le menu Editions par bureau va vous permettre d'éditer l'ensemble des éditions nécessaires au bon déroulement d'une élection.

.. figure:: a_edition_par_bureau.png

     Editions par bureau

Editions générales
------------------
Le menu Editions Générales va vous permettre d'éditer la liste électorale, les cartes d'électeurs, les statistiques généraux, le registre général des procurations, la liste des électeurs inscrits en centre de vote et les étiquettes des électeurs

.. figure:: a_edition_generales.png

     Editions générales


.. _module_carte_electorale:

Module "Cartes Électorales"
---------------------------

.. figure:: a_module_carte_electorale.png

      Module "Cartes Électorales"

     
Éditions pour toutes la commune
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Édition des cartes électorales de la commune par liste (principale ou complémentaires).
Dans cette partie, les éditions doivent être générées avant d'être consultées.


Édition par bureau de vote
~~~~~~~~~~~~~~~~~~~~~~~~~~

Édition des cartes électorales pour une liste et un bureau choisis.
Sur cette édition les cartes peuvent être ordonnées par :
  
  - ORDRE ALPHABÉTIQUE : trie par ordre alphabétique selon le nom et le prénom
  - DATE DE NAISSANCE : trie de la date de naissance la plus ancienne à celle la plus récente
  - VOIE : trie par ordre alphabétique selon le libellé de la voie


Édition par lot
~~~~~~~~~~~~~~~

Édition des cartes électorales à partir du panier de cartes électorales choisies.


Édition par date de naissance
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Édition des cartes électorales pour une liste sélectionnée, en ne gardant que celle pour lesquelles la date de naissance de l'électeur est incluse dans l'intervalle choisi.


Editions Statistiques
---------------------

Le menu Editions statistiques vous permet d'éditer l'ensemble des statistique demandé par La préfecture. Si toutefois vous avez besoin de statistiques complémentaires, vous pour utiliser les exports CSV, à partir du menu Requêtes mémorisées.

.. figure:: a_edition_statistiques.png

     Editions statistiques


Requêtes mémorisées
-------------------

Le menu requêtes mémorisées va vous permettre d'exporter la base électorale afin de vous en servir dans un tableur (excel, openOffice, LibreOffice...).


.. figure:: a_edition_requetes_memorisees.png

     Requêtes memorisées

Prenons un exemple : vous souhaitez sortir l'ensemble de liste électorale sur votre tableur : Cliquez sur "Liste", décochez ou pas les éléments de votre liste, puis dans la partie "paramètre de sortie" choisissez "CSV - export vers le logiciel tableur", puis cliquez sur le bouton "exécuter la requête mémorisée sur liste".

.. figure:: a_edition_sortie_csv.png

     Sortie CSV


