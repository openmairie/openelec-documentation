.. _consultation:

############
Consultation
############

.. figure:: a_menu-rubrik-consultation.png

   Menu - Rubrique 'Consultation'

.. figure:: a_consultation.png

    Exemple de listing de consultation

Pour chaque liste, vous disposez d'une fonction de recherche permettant de trouver un électeur
soit en tapant du texte dans la barre de recherche (recherche simple) soit en sélectionnant plusieurs
critère de recherche (recherche avancée). Pour les menus ayant un formulaire de recherche avancé il est
possible de passer en recherche simple en cliquant sur *Afficher la recherche simple* au dessus
du formulaire de recherche.

Lorsque cela est possible vous trouverez également à gauche de chaque
élément de la liste des boutons permettant de réaliser diverses actions,
comme l'ajout d'un nouvel électeur, la modification, la suppresion ou
encore l'édition de son attestation d'inscription


Consultation de la liste electorale
###################################

La liste electorale est accessible via le menu (:menuselection:`Consultation --> Liste Électorale`).

.. figure:: a_consultation_liste_electorale.png

    Consultation de la liste électorale

Depuis ce menu, il est possible de faire une recherche avec les filtres suivants :

* **Id** : identifiant interne à openElec de l'électeur.
* **INE** : Identifiant National de l'Électeur (REU).
* **Nom** : Nom patronymique / nom d'usage de l'électeur.
* **Prénoms** : Prénoms de l'électeur.
* **Date de naissance** : Date de naissance de l'électeur au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les électeur nés le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Bureau** : Bureau de rattachement de l'électeur.
* **Adresse** : Nom de la voie du bureau de rattachement de l'électeur.
* **Liste** : Liste d'appartenance de l'électeur.
* **Bureau forcé** : Permet de choisir les électeurs pour laquel la sélection du bureau a été forcée (Oui), pas forcé (Non) ou les deux (Tous).

Consultation de la fiche de l'électeur
---------------------------------------

Lorsque vous effectuez une recherche d'électeur par le menu Consultation - Liste électorale, il est possible, en cliquant sur le nom de l'électeur d'accéder à sa fiche.
Cette fiche regroupe l'ensemble des informations concernant l'électeur (état civil, adresse, bureau de vote), l'historique des mouvements appliqués et/ou en cours.
Il vous ai aussi possible d'imprimer sa carte d'électeur, une attestation, effectuer un retour de carte mais aussi saisir un mouvement (radiation, modification)

.. figure:: a_consultation_fiche_electeur.png

     Consultation fiche d'un électeur

Consultation des archives
#########################

Les archives sont accessibles via le menu (:menuselection:`Consultation --> Archive`).

Depuis ce menu il est possible de chercher l'archive d'un électeur à partir de son nom, son prénom,
son nom d'usage, sa date de naissance ou son identifiant dans openElec.

.. figure:: a_consultation_archive.png

    Consultation des archives

Consultation des inscriptions
#############################

Les inscriptions sont accessibles via le menu (:menuselection:`Consultation --> Inscription`).

.. figure:: a_consultation_inscription.png

    Consultation des inscriptions

Depuis ce menu, il est possible de faire une recherche sur les mouvements d'inscriptions d'un électeur avec les filtres suivants :

* **Id** : identifiant interne à openElec de l'électeur.
* **INE** : Identifiant National de l'Électeur (REU).
* **Nom** : Nom patronymique / nom d'usage de l'électeur.
* **Prénoms** : Prénoms de l'électeur.
* **Date de naissance** : Date de naissance de l'électeur au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les électeur nés le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Date de demande** : Date de demande d'inscription au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les demandes faites le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Motif** : Motif d'inscription de l'électeur.
* **Statut** : Statut du mouvement d'inscription (par exemple : ouvert ou en attente).
* **État** : État du mouvement d'inscription (par exemple : traité).
* **Vu ?** : Choix des mouvements d'inscription vu (Oui), non vu (Non) ou les deux (Tous).
* **Liste** : Liste d'appartenance de l'électeur.

Il est également possible d'accéder au formulaire de :ref:`traitement par lot<traitement_par_lot>` en cliquant sur
l'engrenage bleu.

Consultation des modifications
##############################

Les modifications sont accessibles via le menu (:menuselection:`Consultation --> Modification`).

.. figure:: a_consultation_modification.png

    Consultation des modifications

Depuis ce menu, il est possible de faire une recherche sur les mouvements de modification d'un électeur avec les filtres suivants :

* **Id** : identifiant interne à openElec de l'électeur.
* **INE** : Identifiant National de l'Électeur (REU).
* **Nom** : Nom patronymique / nom d'usage de l'électeur.
* **Prénoms** : Prénoms de l'électeur.
* **Date de naissance** : Date de naissance de l'électeur au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les électeur nés le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Date de demande** : Date de demande de modification au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les demandes faites le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Motif** : Motif de modification des informations de l'électeur.
* **Statut** : Statut du mouvement de modification (par exemple : ouvert ou en attente).
* **État** : État du mouvement de modification (par exemple : traité).
* **Vu ?** : Choix des mouvements de modification vu (Oui), non vu (Non) ou les deux (Tous).
* **Liste** : Liste d'appartenance de l'électeur.

Il est également possible d'accéder au formulaire de :ref:`traitement par lot<traitement_par_lot>` en cliquant sur
l'engrenage bleu.

Consultation des radiations
###########################

Les radiations sont accessibles via le menu (:menuselection:`Consultation --> Radiation`).

.. figure:: a_consultation_radiation.png

    Consultation des radiations

Depuis ce menu, il est possible de faire une recherche sur les mouvements de radiation d'un électeur avec les filtres suivants :

* **Id** : identifiant interne à openElec de l'électeur.
* **INE** : Identifiant National de l'Électeur (REU).
* **Nom** : Nom patronymique / nom d'usage de l'électeur.
* **Prénoms** : Prénoms de l'électeur.
* **Date de naissance** : Date de naissance de l'électeur au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les électeur nés le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Date de demande** : Date de demande de radiation au format JJ/MM/AAAA (par exemple : pour récupérer uniquement les demandes faites le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Motif** : Motif de radiation de l'électeur.
* **Statut** : Statut du mouvement de radiation (par exemple : ouvert ou en attente).
* **État** : État du mouvement de radiation (par exemple : traité).
* **Vu ?** : Choix des mouvements de radiation vu (Oui), non vu (Non) ou les deux (Tous).
* **Liste** : Liste d'appartenance de l'électeur.

Il est également possible d'accéder au formulaire de :ref:`traitement par lot<traitement_par_lot>` en cliquant sur
l'engrenage bleu.

Consultation des procurations
#############################

Les procurations sont accessibles via le menu (:menuselection:`Consultation --> Procuration`).

.. figure:: a_consultation_procuration.png

    Consultation des procurations

Depuis ce menu, il est possible de faire une recherche de procuration avec les filtres suivants :

* **Identifiant** : identifiant de la procuration.
* **Mandant** : Identifiant National de l'Électeur (REU).
* **Bureau du mandant** : Bureau d'appartenance du mandant.
* **Mandataire** : Nom patronymique / nom d'usage de l'électeur.
* **Début/Fin** : les dates de validité de la procuration au format JJ/MM/AAAA (par exemple : pour une procuration valide uniquement le 26/05/2019, il faut saisir du 26/05/2019 au 26/05/2019).
* **Statut** : Statut de la procuration (par exemple : demande à transmettre).
* **Vu ?** : Choix des procurations vu (Oui), non vu (Non) ou les deux (Tous).

Il est également possible d'exporter les résultats affichés au format csv.

.. _traitement_par_lot:

Traitement par lot
##################

Le traitement par lot permet d'effectuer un traitement sur l'ensemble des mouvements d'un lot.

L'action de traitement par lot est accessible depuis les menus :

* :menuselection:`Consultation --> Inscription`
* :menuselection:`Consultation --> Modification`
* :menuselection:`Consultation --> Radiation`

Quatre lots sont concernés par ce traitement :

* **Inscriptions acceptées par l'insee** : marque les inscriptions acceptées par l'insee "Non Vu" comme "Vu".
* **Modifications d'état civil** : valide les modifications d'état civil non validées.
* **Radiations d'office** : valide les radiations d'office non validées.
* **Radiations acceptées par l'insee** : marque les radiations acceptées par l'insee "Non Vu" comme "Vu".


En cliquant, sur l'action de consultation par lot (engrenage bleu à côté de la croix verte),
un formulaire permettant de choisir le lot à traiter s'ouvre.

.. figure:: a_consultation_traitement_par_lot.png

    Formulaire de traitement par lot

Lors de la sélection d'un lot, un message indiquant le nombre d'élement qui seront traité s'affiche.
En cliquant, sur le bouton valider le traitement du lot va se déclencher.

.. warning::
    Si de nouveaux éléments sont ajoutés au lot entre le moment où le message indiquant le nombre
    d'élément à traité est affiché et le moment où le traitement est déclenché, il seront traités.
    Par exemple, si j'ai 50 mouvements d'inscription validé par l'INSEE au moment où je
    sélectionne le lot. Le message m'avertira qu'il y a 50 mouvements qui seront traités.
    Si 50 autres mouvements d'inscription validé par l'INSEE sont ajoutés avant que je clique
    sur "Valider" alors lorsque le traitement sera effectué le message me dira que 100 mouvements
    ont été traité.